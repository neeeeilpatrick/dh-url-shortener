let __GROUP_LIST = [];
grouplist.getGroupList();


$(document).ready(function(){

setInterval(()=>{
    $("._7vp:not(.buttonInserted)").each(function(){
        $(this).addClass("buttonInserted");


        // Retrieving important group information
        var id = $(this).parent().attr("id");
        id = id.substr(id.indexOf("Card_")+5, id.length);

        var img = $(this).find("._4-jy img").attr("src");


        // Shorted Group names
        var names = $(this).find("._266w a").text();
        var groupName = names;
        var limitChar = 27;
        if(names.length>limitChar) names = names.substr(0, limitChar)+"...";
        $(this).find("._266w a").html(names);


        // HTML DOM Setup
        var html = "";
        html += "<button data-id='"+id+"' data-name='"+groupName+"' data-img='"+img+"' class='_42ft _4jy0 _p _4jy4 _517h _51sy _4jy2' style='float: left; margin-right: .15rem;'>";
        html += "Add";
        html += "</button>";
        html = $(html);


        // Check if ID already exist and if does change the button layout
        if(grouplist.checkList(id)){
            html.html("Remove");
            html.addClass("added");
        }

        html.click(function(){
            var i = $(this).data("id");
            var n = $(this).data("name");
            var img = $(this).data("img");

            var text = $(this).text();

            if(text=="Add") {
                grouplist.addToList(i, n, img);
                $(this).html("Remove");
                $(this).addClass("added");
            }else{
                grouplist.removeFromList(i);
                $(this).html("Add");
                $(this).removeClass("added");
            }
        });
        $(this).find(".uiPopover").append(html);
    });


    $("._2yau:not(.buttonInserted)").each(function(){
        $(this).addClass("buttonInserted");


        // Retrieving important group information
        var id = $(this).attr("href");
        id = id.replace("/groups/", "");
        id = id.substr(0, id.indexOf("/"));

        var img = $(this).find("img").attr("src");


        // Shorted Group names
        var names = $(this).find("._2yav").text();
        var groupName = names;
        var limitChar = 27;
        if(names.length>limitChar) names = names.substr(0, limitChar)+"...";
        


        if(img!=undefined){
            // HTML DOM Setup
            var html = "";
            html += "<button data-id='"+id+"' data-name='"+groupName+"' data-img='"+img+"' class='_42ft _4jy0 _p _4jy4 _517h _51sy _4jy2' style='float: left; margin: 0 .5rem;'>";
            html += "+";
            html += "</button>";
            html = $(html);


            // Check if ID already exist and if does change the button layout
            if(grouplist.checkList(id)){
                html.html(" - ");
                html.addClass("added");
            }

            html.click(function(){
                var i = $(this).data("id");
                var n = $(this).data("name");
                var img = $(this).data("img");

                var text = $(this).text();

                if(text=="+") {
                    grouplist.addToList(i, n, img);
                    $(this).html(" - ");
                    $(this).addClass("added");
                }else{
                    grouplist.removeFromList(i);
                    $(this).html("+");
                    $(this).removeClass("added");
                }
            });
            $(this).parent().append(html);
        }
    });


    
}, 1000);




});

