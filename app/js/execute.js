

        
$(document).ready(async function(){

        var group = {};

        var url = window.location.href;
        url = url.substr(url.indexOf("?"), url.length);
        url = url.split("&");

        group.id = decodeURIComponent(url[2].substr(url[2].indexOf("=")+1, url[2].length));
        group.text = decodeURIComponent(url[1].substr(url[1].indexOf("=")+1, url[1].length));

        console.log(group);
        let isExist = false;
        
        while(!isExist){
            let a= $("._94t").length;
            if(a!=0) isExist = true;
            await sleep(1);
        }

        $("._94t").find("._55pe").click();
        $(".__MenuItem")[2].click();

        while($("input[name='audience_targets']").length==0) await sleep(1);
        $("input[name='audience_targets']").val(group.id);
        
        $("button[name='__CONFIRM__']").removeAttr("disabled");
        $(".mentionsTextarea").html(group.text);
        $(".mentionsHidden").val(group.text);
        $("button[name='__CONFIRM__']").click();
    });

async function sleep(t){
    return new Promise((resolve, reject)=>{
        let clock = setInterval(()=>{
            t--;

            if(t==0){
                clearInterval(clock);
                resolve();
            }
        }, 1000);
    });
}