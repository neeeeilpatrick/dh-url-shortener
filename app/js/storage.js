
let grouplist = {
    removeFromList: function(id){
        var temp = [];

        for(var i=0; i<__GROUP_LIST.length; i++){
            if(id!=__GROUP_LIST[i].id) temp.push(__GROUP_LIST[i]);
        }

        __GROUP_LIST = temp;
        this.saveList();
    },

    getGroupList: function(){
        chrome.storage.local.get("groupRepository", function(e){
            __GROUP_LIST = e.groupRepository;
            if(__GROUP_LIST == undefined || __GROUP_LIST == null) __GROUP_LIST = [];
            console.log(__GROUP_LIST);
        });
    },
    
    saveList: function(){
        chrome.storage.local.set({groupRepository: __GROUP_LIST}, function(){
            grouplist.getGroupList();
        });
    },

    checkList: function(id){
        var isExist = false;
        for(var i=0; i<__GROUP_LIST.length; i++){
            if(id==__GROUP_LIST[i].id) isExist = true;
        }
        return isExist;
    },

    addToList: function(id, name, img){
        if(!this.checkList(id)){
            __GROUP_LIST.push({id: id, name: name, img: img});
           this.saveList();
        }
    }
    

}


