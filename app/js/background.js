chrome.storage.local.get("groupRepository", function(e){
    if(e.groupRespository==undefined || e.groupRespository==null){
        chrome.storage.local.set({groupRespository: []});
    }
});

chrome.browserAction.onClicked.addListener(function(){
    chrome.tabs.create({url: chrome.runtime.getURL("html/index.html")});
});



chrome.runtime.onConnect.addListener(function(port){
    if(port.name=="post"){
        port.onMessage.addListener(async function(msg){
            switch(msg.action){
                case "start-post":
                    console.log("Posting to "+msg.group.id);
                    port.postMessage({action: "update", id: msg.group.id});
                    await post(msg.group);
                    break;
            }
        });
    }
});

chrome.runtime.onMessage.addListener(function(req, sender, sendRes){
    if(req.action=="close") chrome.tabs.remove(sender.tab.id);
});

async function post(group){
        chrome.tabs.create({url: "https://www.facebook.com/sharer.php?u="+group.link+`&a=${group.text}&b=${group.id}`, selected: false}, function(tabs){
            chrome.tabs.executeScript(tabs.id, {file: "js/library/jquery.js"}, function(){
                chrome.tabs.executeScript(tabs.id, {file: "js/execute.js"}, function(response) {
                    return Promise.resolve();
                }); 
            });
        });
}