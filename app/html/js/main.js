let _$ = {
    updateMenuTab: (firstIndex = false)=>{
        $("#tabMenuList").html("");

        let storage = localStorage.getItem("tabs");
        storage = (storage==null ? [] : JSON.parse(storage));
        
        if(storage.length==0) $("#tabMenuList").html("<li class='no-tabs'>No tabs found</li>");
        else{
            storage.forEach(element => { 
                let active = "";
                if(firstIndex){
                    _$.updateDisplay(element.name);
                    firstIndex = false;
                    active = "active";
                }
                
                let html = $("<li class='menu-item "+active+"'>"+element.name+"</li>");
                html.click(function(){
                    $(".group-tabs").show();
                    $(".link-shortener").hide();
                    $(".active").removeClass("active");
                    $(this).addClass("active");
                    $("input[name='active-tab']").val($(this).text());
                    _$.updateDisplay($(this).text());

                });
                $("#tabMenuList").append(html); 
            });
        }
    },

    activateListener: ()=>clickListener(),

    retrieveData: ()=>{
        let data = localStorage.getItem("tabs");
        return (data==null || data==undefined ? [] : JSON.parse(data));
    },


    updateDisplay: async(item)=>{
        let data;
        _$.retrieveData().forEach(elem=>{ if(elem.name==item) data = elem});

        $(".tab-link").html("");
        $(".load").html('<div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>');
        $(".main").html("<div class='spinner-border' role='status'><span class='sr-only'>Loading...</span></div><div class='no-group'>LOADING...</div>");
        await _$.sleep(1);

        $(".tab-name").html(data.name);
        $(".tab-link").html(data.link);
        $(".tab-link").attr("href", data.link);

        $(".main").html("");
        if(data.data==undefined || data.data.length==0){
            $(".main").html("<div class='no-group'>NO GROUPS ADDED</div>");
        }else{
            data.data.forEach(async elem=>{
                let html = `<div class='launch-group row' data-group='${elem.id}'>`;
                html += "<span class='close-launch-group'>x</span>";
                html += "<div class='launch-group-img'>";
                html += "<img src=''>";
                html += "<span>TITLE</span>";
                html += "</div>";
                html += "<div class='launch-group-text'>";
                html += "<textarea class='form-control'></textarea>";
                html += "</div>";
                html += "</div>";

                html = $(html);

                let name = (elem.name.length>35 ? elem.name.substr(0, 35) : elem.name);
                html.find(".launch-group-img").find("span").html(name);
                html.find(".launch-group-img").find("img").attr("src", elem.img);
                html.find("textarea").html(elem.text);
                
                html.find(".close-launch-group").click(function(){
                    let id = $(this).parent().data("group");
                    $(this).parent().remove();

                    let repo = _$.retrieveData();
                    let current = $("input[name='active-tab']").val();

                    
                    repo.forEach(i=>{
                        if(i.name==current){
                            let p = [];
                            i.data.forEach(o=>{ if(o.id!=id) p.push(o); });
                            i.data = p;
                        }
                    });

                    localStorage.setItem("tabs", JSON.stringify(repo));

                    if($(".launch-group").length==0) $(".main").html("<div class='no-group'>NO GROUPS ADDED</div>");
                });


                html.find("textarea").change(async function(){
                    $(".change-status").html("Saving...");
                    let repo = _$.retrieveData();
                    let current = $("input[name='active-tab']").val();
                    let group = $(this).parent().parent().data("group");
                    
                    
                    repo.forEach(i=>{
                        if(i.name==current) i.data.forEach(o=>{ if(o.id==group) o.text = $(this).val(); });
                    });

                    localStorage.setItem("tabs", JSON.stringify(repo));
                    await _$.sleep(1);
                    $(".change-status").html("Updated!");
                });

                $(".main").append(html);
                
            });
        }
    },

    sleep: (t)=>{
        return new Promise((resolve, reject)=>{
            let clock = setInterval(()=>{
                t--;

                if(t==0){
                    clearInterval(clock);
                    return resolve();
                }
            }, 500);
        });
    }
};




$(document).ready(function(){

_$.updateMenuTab();
_$.activateListener();
shortener();
});


function groupList(callback){
    chrome.storage.local.get("groupRepository", function(e){
       var __GROUP_LIST = e.groupRepository;
        if(__GROUP_LIST == undefined || __GROUP_LIST == null) __GROUP_LIST = [];
        return callback(__GROUP_LIST);
    });
}


function escapeHtml(text) {
    var map = {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      "'": '&#039;'
    };
  
    return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}

$("#sendButton").click(function(){
    var port = chrome.runtime.connect({name: "post"});
    var link = $(".tab-link").attr("href");
    var groups = [];
    $(".launch-group").each(async function(){
        var id = $(this).data("group");
        var text = $(this).find(".launch-group-text").find("textarea").val();
        await port.postMessage({action: "start-post", group: {id: id, link: link, text: text}});
    });
}); 