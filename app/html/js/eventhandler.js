function clickListener(){
    
$("#createTab").click(()=>{
    let tabName = $("input[name='tabName']").val();
    let link = $("input[name='link']").val();
    let isExist = false;

    let storage = localStorage.getItem("tabs");
    storage = (storage==null ? [] : JSON.parse(storage));

    storage.forEach(element => { if(element.name==tabName) isExist = true; });
    
    if(tabName.trim()=="" || link.trim()=="") alert("Please enter all text fields");
    else{
        if(!isExist){
            storage.push({name: tabName, link: link});
            localStorage.setItem("tabs", JSON.stringify(storage));
            
            _$.updateMenuTab();
            $("#cancelTab").click();
        }else alert("Tab name exist");
    }
});



$(".add-button, .refresh").click(function(){
    $("#groupList").html("");

    let selectGroup = [];
    $(".launch-group").each(function(){
        selectGroup.push($(this).data("group"));
    });

    groupList(list=>{
        let showed = 0;
        list.forEach(elem=>{
            var groupName = (elem.name.length>40 ? elem.name.substr(0, 30) : elem.name);
            
            var html = "";
            html += "<div class='group' id='"+elem.id+"' data-name='"+elem.name+"'>";
            html += "<img src='"+elem.img+"' class='group-img'>"+groupName;
            html += "<button class='group-btn' data-id='"+elem.id+"'>Add</button>";
            html += "</div>";

            html = $(html);

            html.find(".group-btn").click(function(){
                let id = $(this).data("id");

                if($(this).text()=="Add"){
                    $(this).text("Remove");
                    $("#"+id).attr("data-included", "true");
                }else{
                    $(this).text("Add");
                    $("#"+id).attr("data-included", "false");
                }
            });

            let isExist = false;
            selectGroup.forEach(l=>{if(l==elem.id) isExist = true});

            if(!isExist){
                $("#groupList").append(html);
                showed++;
            }
            
        });

        if(showed==0){$("#groupList").append("<div class='group'>No groups found</div>");}
        
    });
});


$("#addText").click(()=>{ $("#txtArea").append("<textarea class='form-control'></textarea>"); });
$("#addGroup").click(()=>{
    let groups = [];
    let text = [];

    $("#txtArea").find("textarea").each(function(){ text.push($(this).val()); });

    $(".group").each(function(){
        if($(this).data("included")==true){
            let id = $(this).attr("id");
            let name = $(this).data('name');
            let img = $(this).find("img").attr('src');
            let rand = Math.floor((Math.random() * text.length));
            groups.push({id: id, name: name, img: img, text: text[rand]});
        }
    });

    let current = $("input[name='active-tab']").val();
    let data = _$.retrieveData();
    data.forEach(elem=>{
        if(current==elem.name){
            if(elem.data==undefined) elem.data = groups;
            else{
                groups.forEach(a=>{ elem.data.push(a); });
            }
        }
    });

    localStorage.setItem("tabs", JSON.stringify(data));
    $("#cancelGroup").click();
    _$.updateDisplay(current);

});


$("#editButton").click(function(){
    $("#editGroup").find("input[name='tabName']").val($("input[name='active-tab']").val());
    $("#editGroup").find("input[name='link']").val($(".tab-link").text());
});

$("#updateButton").click(function(){
    let name = $("#editGroup").find("input[name='tabName']").val();
    let current = $("input[name='active-tab']").val();
    let data = _$.retrieveData();
    data.forEach(elem=>{
        if(current==elem.name){
            elem.name = name;
            elem.link = $("#editGroup").find("input[name='link']").val();
        }
    });

    localStorage.setItem("tabs", JSON.stringify(data));
    $("input[name='active-tab']").val(name);
    _$.updateMenuTab(false);
    _$.updateDisplay(name);
});

$("#deleteButton").click(function(){
    let current = $("input[name='active-tab']").val();
    let data = _$.retrieveData();
    let temp = [];
    data.forEach(elem=>{
        if(current!=elem.name) temp.push(elem);
    });

    $("#deleteCancel").click();
    localStorage.setItem("tabs", JSON.stringify(temp));
    _$.updateMenuTab(true);

});

$("#shortener").click(()=>{shortener()});


$(".sign-out").click(function(){
    localStorage.setItem("license", "");
    window.location.reload();
});


$(".tracking-code-open").click(function(){
    var pixel = localStorage.getItem("facebook-pixel");
    
    $(".tracking-code-form").show(250);
    if(pixel!=undefined) $("input[name='system-facebook-pixel']").val(pixel);

});

}

