let endpoint = "http://54.158.210.177:8080";
let FOLDERS = [];
let SOCKET;
let ACCOUNT = [];
let LINKS = [];

async function shortener(){
    $(".group-tabs").hide();
    $(".link-shortener").show();

    
    if(localStorage.getItem("license")==null || localStorage.getItem("license")=="") nouser();
    else{
        var initiateSocket = true;
        if(SOCKET!=undefined) if(SOCKET.readyState===SOCKET.OPEN) initiateSocket = false;
        

        console.log(initiateSocket);
        if(initiateSocket){
            SOCKET = io.connect(endpoint);
            
            SOCKET.on("connect", ()=>{
                SOCKET.emit("user-validate", localStorage.getItem("license"), async (data)=>{
                    if(data.status=="true"){
                        LINKS = data.data.data;
                        ACCOUNT = data.data;
                        await login();
                        
                        $(".account-email").html(ACCOUNT.email);
                        updateData(data.data.data);

                    }
                    else nouser();
                });
            });
            

          
        }else{
            window.history.pushState(null, '', 'index.html'); 
            updateData(LINKS);
        }
        

        

        
        
    }
}


function updateData(links){
    var LOGS = [];
    FOLDER = [];
    $("#linkList").html("");

    (links==undefined ? links = [] : false);
    if(links.length!=0){
        links.forEach(function(link){


            // CHECK and ADD to folder
            var isFolderExist = false;
            FOLDER.forEach(function(folder){ if(folder.name==link.folder){isFolderExist = true; folder.count++; }});
            if(isFolderExist==false && link.folder!="") FOLDER.push({name: link.folder, count: 1});

            // FOLDER FILTER
            var isDisplay = true;
            if(window.location.href.indexOf("folder")!=-1){
                var url = window.location.href;
                var folderFilter = url.substr(url.indexOf("folder=")+("folder=").length, url.length);

                folderFilter = decodeURI(folderFilter);
                console.log(folderFilter);
                (folderFilter.indexOf("&")!=-1 ? folderFilter = folderFilter.substr(0, folderFilter.indexOf("&")) : false);
                if(folderFilter.trim()!=""){
                    if(link.folder.toLowerCase()!=folderFilter.toLowerCase()) isDisplay = false;
                }
            }
        
            if(isDisplay){
                    // DISPLAY LIST
                    var date = new Date(link.date);
                    var currentDate = new Date();
                    var year = (date.getFullYear()==currentDate.getFullYear() ? "" : ", "+date.getFullYear());
                    var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                    date = `${month[date.getMonth()]} ${date.getDate()}${year}`;
    
                    var tableLinks = "";
                    tableLinks += `<tr>`;
                    tableLinks += `<td class='text-center'>${date}</td>`;
                    tableLinks += `<td class='text-left'>`;
                    tableLinks += `<span><a href='http://masterl.ink/${link.id}' target='_blank'>http://masterl.ink/${link.id}</a></span> <i class='far fa-clone copy'></i><br>`;
                    tableLinks += `<span>${link.target}</span>`;
                    tableLinks += `</td>`;
                    
                    if(link.folder!="")tableLinks += `<td class='text-center' id='folder' data-folder='${link.folder}'><span class='folder'>${link.folder}</span></td>`;
                    else tableLinks += `<td class='text-center' id='folder'>No Folder</td>`;
    
                    // Clicks
                    var clicks = 0;
                    if(link.logs!=undefined) clicks = link.logs.length;
    
                    tableLinks += `<td class='text-center'>${clicks}</td>`;
                    tableLinks += `<td class='text-center'><i class='fas fa-ellipsis-h settings' data-id='${link.id}' data-folder='${link.folder}' data-target='${link.target}' data-recipients='${link.recipients}' data-facebook-pixel='${link.facebook_pixel}'></i></td>`;
                    tableLinks += `</tr>`;
    
                    var item = $(tableLinks);
    
                    
    
                    item.find(".copy").click(function(){
                        var shortlink = $(this).parent().find("a").html();
                        copyTextToClipboard(shortlink);
                    });
    
                    item.find(".settings").click(function(){
                        var id = $(this).data("id");
                        var folder = $(this).data("folder");
                        var target = $(this).data("target");
                        var recipients = ($(this).data("recipients")!=null ? $(this).data("recipients").split(",") : ["", ""]);
                        var facebook_pixel = $(this).data("facebook-pixel");
    
                        if(ACCOUNT.email==recipients[0]){
                            $("input[name='default-email']").prop("checked", true);
                            $("input[name='email_1']").attr("disabled", "disabled");
                        }else $("input[name='default-email']").prop("checked", false);

                        var system_default_fb_pixel = localStorage.getItem("facebook-pixel");

                        if(facebook_pixel==system_default_fb_pixel){
                            $("input[name='default-fb-pixel']").prop("checked", true);
                            $("input[name='fb_pixel']").attr("disabled", "disabled");
                        }else $("input[name='default-fb-pixel']").prop("checked", false);
    
                       
                        $("input[name='original-key']").val(id);
                        $("input[name='original-link']").val(target);
                        $(".short-link").val(id);
                        $(".fb_pixel").val((facebook_pixel==undefined ? "" : facebook_pixel));
                        $(".link-custom").hide();
                        $(".delete-link").show();
                        
                        $("#selectFolder").val(folder);
                        $("#inputFolder .input").val(folder);
    
                        $(".link-long").html(target);
                        $(".link-long").hide();
                        $("input[name='target-link']").val(target).show();  
    
                        $("#createLink").hide();
                        $("#updateLink").show();
                        $("#linkForm").click();
    
                        
                        if(recipients[0]!=undefined && recipients[0]!="undefined"){
                            $("input[name='email_1']").val(recipients[0]);
                            $("input[name='email_2']").val(recipients[1]);
                        }else{
                            $("input[name='email_1']").val("");
                            $("input[name='email_2']").val("");
                        }
    
                        $(".update-notification").hide();
                        $(".delete-confirmation").hide();
                        $(".form-block").show();
                    });
    
                    $("#linkList").prepend(item);
    
                    if(link.logs!=undefined)
                        link.logs.forEach(function(t){ 
                            var logDate = new Date(t.date);
                            LOGS.push({ip: t.ip, date: `${logDate.getFullYear()}-${logDate.getMonth()+1}-${logDate.getDate()}`});
                        });
    
            }
        });


    }else{
        $("#linkList").html("<tr style='background: none;'><td colspan='5' style='    border-bottom: 10px solid #f8f9ff;padding: 1rem;font-weight: bold;color: #969696;font-size: 11pt;'>NO LINKS FOUND!</td></tr>");
    }

    // UPDATE SELECT FOLDER 
    var options = `<option value=""></option>`;
    var menus = ``;
    if(FOLDER.length==0) options += `<option disabled>No folders exist</option>`;
    else{
        FOLDER.forEach(function(folder){
            menus += `<li class='folders' data-folder='${folder.name}'><i class="far fa-folder"></i><span style="margin-left: .5rem;">${folder.name}</span><span class='link-counter'>${folder.count}</span></li>`;
            options += `<option value='${folder.name}'>${folder.name}</option>`;
        });
    }

   
    options += `<option value='add-new'>Add New Folder</option>`;
    $("#selectFolder").html(options);
    $(".link-folders").html(menus);

    $(".link-folders").find(".folders").click(function(){
        var selectedFolder = $(this).data("folder");
        window.history.pushState(null, '', 'index.html?folder='+selectedFolder); 
        updateData(links);
    });
    $(".folder-counter").html(links.length);
    

    // Process Analytics
    var chartLog = [];
    if(LOGS.length!=0){
        LOGS.forEach(function(log){
            var chartExist = false;
            var index = 0;
            chartLog.forEach(function(chart){
                if(chart.x==new Date(log.date).toString()) {
                    chartExist = true;
                }

                if(!chartExist) index++;
            });
            
            if(chartExist)  chartLog[index].y++;
            else chartLog.push({x: new Date(log.date), y: 1});
            

        });
        renderAnalytics(chartLog, [{x: new Date(), y:0}]);
    }else renderAnalytics([{x: new Date(), y:0}], [{x:new Date(), y:0}]);
    
    //
}

function copyTextToClipboard(text) {
    if (!navigator.clipboard) {
      fallbackCopyTextToClipboard(text);
      return;
    }
    navigator.clipboard.writeText(text);
}


function load(status){
    if(status){
        $(".link-loader").css("display", "inline-block");
        $("#createLink").attr("disabled", "disabled");
        $("#createLink").html("Loading");
    }else{
        $(".link-loader").css("display", "none");
        $("#createLink").removeAttr("disabled");
        $("#createLink").html("Create Link");
    }
}


function formReset(){
    $("input[name='folder']").val("");
    $("input[name='long-link']").val("");
    $("#selectFolder").show();
    $("#inputFolder").hide();
    $(".short-link").val("");
}


function toast(msg, type){
    if(type==undefined || type==null) type = "error";
    if(msg=="" || msg==false) $(".toast").hide(300);
    else {
        $(".toast").removeClass(`toast-error`);
        $(".toast").removeClass(`toast-success`);
        $(".toast").addClass(`toast-${type}`);
        $(".toast-msg").html(msg);
        $(".toast").show(300);
        $(".toast, .toast-close").click(()=>{
            $(".toast").hide(300);
        });
    }
}


function validLink(key) { 
    var re = /^[0-9-A-z-_ -]*$/; 
    if (!re.test(key)) return false;
    else return true;
} 

function validURL(value) { return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(value);
}


async function nouser(){
    let license = await fetch("nouser.html");
    license = await license.text();
    license = $(license);

    $(".menu-bottom-option").hide();

    license.find("#validateLicense").click(async function(){
        let key = $(this).parent().find("input[name='license']").val();
        
        
        $(".hide-validation").fadeOut('400');
        $(".validate-status").html(`<div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div> Validating product key`);
        
        var validate = {status: ""};
        if(key.trim()!="") {
            validate = await fetch(`${endpoint}api/validate/${key}`);
            validate = await validate.json();
        }

        if(validate.status=="false" || key.trim()==""){
            $(".validate-status").html(`<div class="alert alert-danger" role="alert">Invalid product key!</div>`);
            $(".hide-validation").fadeIn('400');
        }else{
            $(".validate-status").html(`<div class="alert alert-success" role="alert">Loading resources</div>`)
            localStorage.setItem("license", key);

            $(".menu-bottom-option").show();
            shortener();
        }
        
    });
    $(".link-shortener").html(license);
}


async function login(){
    let shortener = await fetch("shortener.html");
    shortener = await shortener.text();
    shortener = $(shortener);

    shortener.find("#createShortLink").click(function(){
        var website = $("input[name='longlink']").val();
        
        if(validURL(website)){
            toast(false);

            $(".link-long").html(website).show();
            $("input[name='target-link']").val(website).hide();
            $(".link-status").hide();
            $(".link-custom").show();
            formReset();
            $(".delete-link").hide();
            $("#linkForm").click();
            
            $("#updateLink").hide();
            $("#createLink").show();

            $(".form-block").show();
            $(".delete-confirmation").hide();
            $(".update-notification").hide();
        }else if(website.trim()=="") toast("<i class='fas fa-exclamation-circle'></i> Please enter a valid url");
        else toast(`<i class='fas fa-exclamation-circle'></i> Bad Link! Bad Link!`);
    });

    shortener.find("#selectFolder").on("change", function(){
        var folder = $(this).val();

        if(folder=="add-new"){
            $("#selectFolder").hide();
            $("#inputFolder").show();
        }else $("#inputFolder .input").val(folder);
    });

    

    shortener.find(".cancel-folder").click(function(){
        $("#selectFolder").show();
        $("#inputFolder").hide();
        $("#selectFolder").val("");
    });

    shortener.find(".tracking-close").click(function(){
        $(".tracking-code-form").hide(250);
    });
    
    shortener.find(".tracking-save").click(function(){
        var pixel = $("input[name='system-facebook-pixel']").val();
        localStorage.setItem("facebook-pixel", pixel);
        $(".tracking-code-form").hide(250);
    });

    
    

    shortener.find("#createLink").click(function(){
        var key = $(".short-link").val();
        var longLink = $("input[name='target-link']").val();
        var folder = $("input[name='folder']").val();
        var email1 = $("input[name='email_1']").val();
        var email2 = $("input[name='email_2']").val();
        var facebook_pixel = $("input[name='fb_pixel']").val();

        if(validLink(key)){
            load(true);

            
            var socket_data = {
                key: key,
                link: longLink, 
                folder: folder,
                fbPixel: facebook_pixel,
                recipients: [email1, email2]
            };

            SOCKET.emit("register-link", JSON.stringify(socket_data), async (data)=>{
                
                if(data.status==false) SOCKET_PROCESS.linkDuplicate(data);
                else SOCKET_PROCESS.displayData(data);
                
            });
            
            $("input[name='longlink']").val("");
        }else{
            $(".link-status").html(`It can only contain the characters "-,_,a-z,A-Z,0-9"`).show();
        }
    });

    shortener.find("input[name='default-email']").click(function(){
        if($(this).prop("checked")){
            $("input[name='email_1']").val(ACCOUNT.email).attr("disabled", "disabled");
        }else $("input[name='email_1']").val("").removeAttr("disabled");

    });

    shortener.find("input[name='default-fb-pixel']").click(function(){
        if($(this).prop("checked")){
            var pixel = localStorage.getItem("facebook-pixel");

            if(pixel!=undefined && pixel!=""){
                $("input[name='fb_pixel']").val(pixel).attr("disabled", "disabled");
            }
            
        }else $("input[name='fb_pixel']").val("").removeAttr("disabled");

    });
    
    shortener.find("#confirmChange").click(function(){
        var includeEmail = $("input[name='reports']").prop("checked");
        var key = $(".short-link").val();
        var longLink = $("input[name='target-link']").val();
        var folder = $("#inputFolder").find("input[name='folder']").val();
        var originalKey = $("input[name='original-key']").val();
        var recipients = [$("input[name='email_1']").val(), $("input[name='email_2']").val()];
        var facebook_pixel = $("input[name='fb_pixel']").val();
        load(true);

        var socket_data = JSON.stringify({
            key: key,
            link: longLink, 
            folder: folder,
            sendReport: includeEmail,
            originalKey: originalKey,
            fbPixel: facebook_pixel,
            recipients: recipients
        });

        SOCKET.emit("update-link", socket_data, async (data)=>{
            if(!data.status) SOCKET_PROCESS.linkDuplicate(data);
            else SOCKET_PROCESS.displayData(data);
        });

        
    });

    shortener.find(".delete-link").click(function(){
        $(".delete-confirmation").show();
        $(".form-block").hide();
        $(".delete-id").html(`masterl.ink/${ $("input[name='original-key']").val() }`);
    });
    

    shortener.find("#deleteLink").click(function(){
        var key = $("input[name='original-key']").val();

        SOCKET.emit("delete-link", key, async (data)=>{
            if(data.status==true) SOCKET_PROCESS.displayData(data);
            else {
                $("#createClose").click();
                toast(`<i class='fas fa-exclamation-circle'></i> ${data.message}`);
                
            }
        });

    });


    shortener.find("#updateLink").click(function(){
        var key = $(".short-link").val();
        var longLink = $("input[name='target-link']").val();
        var folder = $("#inputFolder").find("input[name='folder']").val();
        var originalKey = $("input[name='original-key']").val();
        var originalTarget = $("input[name='original-link']").val();
        var recipients = [$("input[name='email_1']").val(), $("input[name='email_2']").val()];
        var facebook_pixel = $("input[name='fb_pixel']").val();
        
        if(!validURL(longLink)) $(".target-status").html(`Please enter a valid url`).show();
        if(validLink(key) && validURL(longLink)){

            if(originalTarget==longLink){
                
                load(true);
                var socket_data = JSON.stringify({
                    key: key,
                    link: longLink, 
                    folder: folder,
                    sendReport: false,
                    originalKey: originalKey,
                    fbPixel: facebook_pixel,
                    recipients: recipients
                });
        
                SOCKET.emit("update-link", socket_data, async (data)=>{
                    if(!data.status) SOCKET_PROCESS.linkDuplicate(data);
                    else SOCKET_PROCESS.displayData(data);
                });

            }else{
                $(".update-email").html("");
                var emailNumber = 0;
                recipients.forEach(function(email){
                    if(email.trim()!=""){
                        $(".update-email").append(`<div>${email}</div>`);
                        emailNumber++;
                    }
                });

                if(emailNumber==0){
                    $(".update-email").html(`<div>No emails added</div>`);
                }


                $(".update-notification").find(".update-target-link").html(longLink);
                $(".update-notification").find(".update-short-link").html(`masterl.ink/${key}`);
                $(".update-notification").show();
                $(".form-block").hide();
            }
            

        }else{
            $(".link-status").html(`It can only contain the characters "-,_,a-z,A-Z,0-9"`).show();
        }
    });
    $(".link-shortener").html(shortener);
}




function renderAnalytics(clicks, uniqueClicks){
    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        theme: "light2",
        axisX:{
            valueFormatString: "DD MMM",
            crosshair: {
                enabled: true,
                snapToDataPoint: true
            }
        },
        axisY: {
            title: "Number of Visits",
            crosshair: {
                enabled: true
            }
        },
        toolTip:{
            shared:true
        },  
        legend:{
            cursor:"pointer",
            verticalAlign: "bottom",
            horizontalAlign: "left",
            dockInsidePlotArea: true,
            itemclick: toogleDataSeries
        },
        data: [{
            type: "line",
            showInLegend: true,
            name: "Total Visit",
            markerType: "square",
            xValueFormatString: "DD MMM, YYYY",
            color: "#F08080",
            dataPoints: clicks
         }
        // {
        //     type: "line",
        //     showInLegend: true,
        //     name: "Unique Visit",
        //     lineDashType: "dash",
        //     dataPoints: uniqueClicks

        // }
        ],
        backgroundColor: "#f8f9ff"
    });
    chart.render();

    function toogleDataSeries(e){
        if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        } else{
            e.dataSeries.visible = true;
        }
        chart.render();
    }


    
}



var SOCKET_PROCESS = {
    displayData: (data)=>{
        LINKS = data.links;

        if(data.tail=="delete") toast("Link deleted", "success");
        if(data.tail=="no-link-id") toast("Please enter a valid shortlink!", "error");
        

        updateData(data.links);
        formReset();
        load(false);
        $("#createClose").click();
    },
    linkDuplicate: (data)=>{
        $(".link-status").html(`masterl.ink/${data.key} already exist `).show();
        load(false);
    }
}