const MongoClient = require("mongodb").MongoClient;
const ObjectId = require("mongodb").ObjectID;
const MongoURL = "mongodb://54.86.27.45:27017/";
const tool = require("./tools");

let db;


module.exports = {
    connect: ()=>{
        return new Promise((resolve, reject)=>{
            MongoClient.connect(MongoURL, {useNewUrlParser: true}, async (err, client)=>{
                if(err) throw err;
            
                db = client.db("masterlink");
                try{
                    db.createCollection("users");
                    
                }catch(err){
                    
                }

                resolve();
            });
        })
    },

    checkProduct: async function(id){
        var products = await this.extractData("products", {productID: id});

        if(products.length==0) return false;
        else return products;
    },

    checkUser: async function(id){
        var users = await this.extractData("users", {customer: id});

        if(users.length==0) return false;
        else return users[0];
    },

    getCustomer: async function(id){
        var user = await this.extractData("users", {license: id});

        if(user.length==0) return false;
        else return user;
    },

    getCustomerLinks: async function(id){
        var links = await this.extractData("users", {customer: id});
        return links;
    },

    deleteLink: async function(user, id){
        var links = await this.extractData("users", {customer: user});
        links = links[0].data;
        (links == undefined ? links = [] : false);
        
        var temp = [];
        links.forEach(function(link){
            if(link.id!=id) temp.push(link);
        });

        await this.updateCustomerLinks(user, temp);
        return await this.extractData("users", {customer: user});        
    },

    addCustomer: async function(id, email){
        await db.collection("users").insertOne({customer: id, email: email})
    },

    updateCustomerLinks: async function(id, links){
        return await db.collection("users").updateOne({customer: id}, {$set: {data: links}});
    },


    updateCustomer: async function(data){
        return await db.collection("users").updateOne({customer: data.customer}, {$set: {
            license: data.receipt,
            active: true,
            purchase_date: data.invoice_date,
            last_payment_date: data.period.start,
            expiration_date: data.period.end,
        }});
    },

    updateInvoiceList: async function(data){
        var invoices = await db.collection("users").find({customer: data.customer}).toArray();
        invoices = invoices[0];

        let list = [];
        if(invoices.invoices!=undefined) list = invoices.invoices;

        let isExist = false;
        list.forEach(invoice => {if(data.invoice==invoice.invoice) isExist = true;});
        if(!isExist) list.push({invoice: data.invoice, date: data.invoice_date});

        return await db.collection("users").updateOne({customer: data.customer}, {$set: {invoices: list}});
    },

    extractData: async function(col, settings){
        return await db.collection(col).find(settings).toArray();
    },

    getRedirect: async function(id){
        return await db.collection("users").find({'data.id': {$regex: id}}).toArray();
    }
}

