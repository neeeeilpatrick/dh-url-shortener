const express = require("express");
const database = require("./database");
var router = express.Router();
const WebSocket = require("ws");
const email = require("./ses");
const tool = require("./tools");


router.get("/validate/:id", async function(request, response){
    var result = await validateLicense(request.params.id);
    response.json(result);
});

module.exports = router;

async function validateLicense(id){
    tool.logs(`(LOGIN) ${id}`);
    let info = await database.getCustomer(id);
    
    var result; 
    if(info==false) result = {status: "false"};
    else{
        var temp = {};

        temp.email = info[0].email;
        temp.expiration = info[0].expiration_date;
        temp.last_payment = info[0].last_payment_date;
        temp.customer = info[0].customer;
        temp.data = info[0].data;

        result = {status: "true", data: temp};
        tool.logs(`(CONNECTED) ${temp.customer}`);
    }

    return result;
}

