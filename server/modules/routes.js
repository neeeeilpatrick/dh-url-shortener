const express = require("express");
const database = require("./database");
const ipinfo = require("ipinfo");
const path = require("path");
var router = express.Router();
const tool = require("./tools");


router.get("/", function(request, response){
    response.send("Welcome to Masterl.ink");
});

router.get("/:id", async function(request, response){
    let id = request.params.id;
    let link = await database.getRedirect(id);
    var ip = request.headers['x-forwarded-for'] || request.connection.remoteAddress;
    
    ip = ip.replace("::ffff:", "");

    if(link.length==0) response.sendStatus(404);
    else{
        urls = link[0].data;
        customer = link[0].customer;

        
        urls.forEach(async url => {
            if(url.id==id){
                var pattern = /^((http|https|ftp):\/\/)/;
                link = url.target;
                if(!pattern.test(link)) {
                    link = "http://" + link;
                }

                
                if(url.logs==undefined) url.logs = [];
                ipinfo(ip, async function(err, info){
                    info.date = new Date();
                    url.logs.push(info);
                    
                    tool.logs(`(PAGE VIEW) /${id}`);
                    await database.updateCustomerLinks(customer, urls);
                    
                    if(url.facebook_pixel!=undefined){
                        if(url.facebook_pixel.trim()!=""){
                            response.header('Content-Type', 'text/html').send(`
                                <html>
                                    <head>
                                            <!-- Facebook Pixel Code -->
                                            <script>
                                            !function(f,b,e,v,n,t,s)
                                            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                                            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                                            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                                            n.queue=[];t=b.createElement(e);t.async=!0;
                                            t.src=v;s=b.getElementsByTagName(e)[0];
                                            s.parentNode.insertBefore(t,s)}(window, document,'script',
                                            'https://connect.facebook.net/en_US/fbevents.js');
                                            fbq('init', '${url.facebook_pixel}');
                                            fbq('track', 'PageView');

                                            
                                            </script>
                                            <noscript>
                                            <img height="1" width="1" style="display:none" 
                                                src="https://www.facebook.com/tr?id=your-pixel-id-goes-here&ev=PageView&noscript=1"/>
                                            </noscript>
                                            <!-- End Facebook Pixel Code -->


                                            <script>
                                                setInterval(function(){

                                                    window.location.href = '${link}';
                                                }, 1500);
                                            </script>
                                            
                                    </head>
                                </html>
                            
                            `);
                        }else{
                            response.writeHead(301, {
                                'Location': link,
                                'Accept-Ranges': 'bytes',
                                'Cache-Control': 'no-cache'
                            });
                            response.end();
                        }
                    }else{
                        response.writeHead(301, {
                            'Location': link,
                            'Accept-Ranges': 'bytes',
                            'Cache-Control': 'no-cache'
                        });
                        response.end();
                    }
                     
                });
            } 
        });

        
    }
});

module.exports = router ;