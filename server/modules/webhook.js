const database = require("./database");
const express = require("express");
var router = express.Router();

router.get("/", async function(request, response){
    let data = require("./stripe");

    //let data = JSON.parse(request)
    if(data.type=="invoice.payment_succeeded"){
        data = data.data.object;
        
        var __purchase_info = {
            invoice: data.id,
            invoice_date: new Date(data.date*1000),
            product: data.lines.data[0].plan.product,
            receipt: data.number,
            subscription: data.lines.data[0].id,
            price: data.amount_due,
            period: {
                start: new Date(data.lines.data[0].period.start*1000),
                end: new Date(data.lines.data[0].period.end*1000)
            },
            customer: "cus_EYKzHP1YMOTVn3" //data.customer
        }
        console.log(__purchase_info);
        
        let users = await database.checkUser(__purchase_info.customer);
            
        /**
         * If user does not exist in the database
         */
        if(!users){
            var stripe = require("stripe")("sk_test_KbdJHzZIr8CXchuQ7tihSWn6");
            await stripe.customers.retrieve(__purchase_info.customer, function(err, customer){
                return new Promise(async (resolve, reject)=>{
                    var email = customer.email;
                
                    await database.addCustomer(__purchase_info.customer, email);
                    await database.updateCustomer(__purchase_info);
                    await database.updateInvoiceList(__purchase_info);
                    resolve();
                });
            });
        }else{
            await database.updateCustomer(__purchase_info);
            await database.updateInvoiceList(__purchase_info);
        }
        
        console.log("Register successful");
        response.sendStatus(200);
    }
    
});


module.exports = router 