module.exports = {
    sendReport:  function(to, link, report){
        return new Promise((resolve, reject)=>{
            var aws = require('aws-sdk');
        aws.config.loadFromPath('./config.json');
        var ses = new aws.SES({apiVersion: '2010-12-01'});
        var from = 'neeeilpatricklacson@gmail.com';
        ses.sendEmail(
        {
            Source: from,
            Destination: { ToAddresses: to },
            Message: {
                Subject: {
                    Data: `Campaign Result - masterl.ink/${ link.short}`
                },
                Body: {
                    Html: {
                            Charset: "UTF-8",
                            Data: `
                            <html>

                                <head>

                                    <style>
                                            

                                            body {
                                                font-family: "Open Sans", sans-serif;
                                            }

                                            .main-header {
                                                text-align: center;
                                                font-weight: 700;
                                                font-size: 15pt;
                                                color: #7e7e7e;
                                            }

                                            .short-link {
                                                padding: .3rem;
                                                border: 2px solid #ea510b;
                                                border-radius: 3px;
                                                font-size: 10pt;
                                                margin: 1.5rem auto .2rem;
                                                width: 80%;
                                                text-align: center;
                                                color: #fff;
                                                font-weight: 800;
                                                background: #ea510b;
                                            }

                                            .long-link {
                                                font-size: 9pt;
                                                color: #000;
                                                text-align: center;
                                                margin-bottom: 3rem;
                                            }

                                            .analytics {
                                                text-align: center;
                                                width: 100%;
                                                border-collapse: collapse;
                                            }


                                            .analytics .data {
                                                font-size: 10pt;
                                                background: #f2f2f2;
                                                border-left: 5px solid #a3a3a3;
                                                border-bottom: 12px solid #fff
                                            }

                                            .analytics .data td {padding: .5rem;}
                                            .analytics .data .title {
                                                text-align: left;
                                                font-weight: bold;
                                            }


                                            .analytics .data .number {
                                                text-align: center;

                                            }
                                    </style>
                                </head>
                                <body>
                                    <div style="background: rgb(240, 240, 240) ;width: 95%; padding:0.2rem">
                                        
                                        <div style="width:50%; background: #fff; margin: 4rem auto; padding: 1rem ">
                                            <div class="main-header"><i class="fas fa-chart-bar"></i> &nbsp;Campaign Report</div>
                                            <div class="short-link">masterl.ink/${ link.short }</div>
                                            <div class="long-link">${ link.long }</div>

                                            <table class='analytics' border="0">
                                                    <tr class="data">
                                                            <td class="title">Total Clicks</td>
                                                            <td class="number">${ report.totalClicks}</td>
                                                    </tr>
                                                    <tr class="data">
                                                        <td class="title">Total Unique Clicks</td>
                                                        <td class="number">${ report.totalUniqueClicks }</td>
                                                    </tr>
                                                    <tr class="data">
                                                        <td class="title">Average Daily Clicks</td>
                                                        <td class="number">${ Math.round(report.dailyAverageClicks) }</td>
                                                    </tr>
                                            </table>
                                        </div>
                                    </div>
                                </body>
                            </html> 
                            `,
                    }
                }
            }
        }, function(err, data){
            if(err==null) resolve();
            else reject();
        });
        });
    },
    
    test: async function(){
        return new Promise((resolve, reject)=>{
            var t = 5;
            var clock = setInterval(function(){
                console.log(t);
                t--;

                if(t==0){
                    clearInterval(clock);
                    resolve();
                }
            }, 1000);  
        });
    }
}