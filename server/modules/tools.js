module.exports = {
    logs: (log) => {
        var date = new Date();
        var y = date.getFullYear();
        var m = leadingZeroes(date.getMonth()+1, 2);
        var d = leadingZeroes(date.getDate(), 2);
        var h = leadingZeroes(date.getHours(), 2);
        var mn = leadingZeroes(date.getMinutes(), 2);
        var s = leadingZeroes(date.getSeconds(), 2);

        log = `${y}-${m}-${d} ${h}:${mn}:${s}  ${log}`;

        console.log(log);
    }
}


function leadingZeroes(num, size){
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
}