
const database = require("./database");
const tool = require("./tools");
const email = require("./ses");

module.exports = {
    SOCKET: (socket) => {
        
        socket.on('connection', function(client){ 

            client.on("user-validate", async (key, callback)=>{
                key = key.trim();
                
                tool.logs(`(LOGIN) ${key}`);
                var result = await validateLicense(key);
                if(result.status=="true"){
                    client.id = result.data.customer;
                    client.license = key;
                    tool.logs(`(CONNECTED) ${client.id}`);
                }
                callback(result);
            });


            client.on("register-link", async (data, callback)=>{
                data = JSON.parse(data);
                
                data.key = data.key.toLowerCase();
                var link = await database.getRedirect(data.key);
                var links; 

                if(data.key=="" || data.key==undefined){
                    links = await database.getCustomerLinks(client.id);
                    links = links[0].data;
                    callback({status: true, links: links.data, tail: "no-link-id"});
                }else{
                    
                    links = await database.getCustomerLinks(client.id);
                    links = links[0].data;

                    if(link.length!=0) callback({status: false, links: links[0].data, key: data.key});
                    else{
                        if(links==undefined) links = [];
                        
                        links.push({
                            date: new Date(),
                            id: data.key,
                            target: data.link,
                            folder: data.folder,
                            recipients: data.recipients,
                            facebook_pixel: data.fbPixel
                        });

                        await database.updateCustomerLinks(client.id, links);
                        links = await database.getCustomerLinks(client.id);

                        tool.logs(`(LINK CREATE) /${data.key} >> SUCCESS`);
                        callback({status: true, links: links[0].data});
                    }
                }
            });


            client.on("delete-link", async (key, callback)=>{
                
                var result = await validateLicense(client.license);
                if(result.status=="true"){
                    var data = await database.getRedirect(key);
                    data = data[0].data;
                    
                    data.forEach(async function(link){
                        if(key==link.id){
                            var emails = [];     
                            link.recipients.forEach(function(email){ if(email!="") emails.push(email);});
                            
                            if(emails.length!=0){
                                try{
                                    await sendCampaignReport(emails, key);
                                    tool.logs(`(LINK DELETE) /${key} >> SUCCESS`);
                                    var userData = await database.deleteLink(client.id, key);
                                    callback({status: true, links: userData[0].data, tail: "delete"});
                                }catch(err){
                                    
                                }
                            }else{
                                tool.logs(`(LINK DELETE) /${key} >> SUCCESS`);
                                var userData = await database.deleteLink(client.id, key);
                                callback({status: true, links: userData[0].data, tail: "delete"});
                            }
                        }
                    });
                    
                    
                }else{
                    tool.logs(`(LINK DELETE) /${key} >> FAILED`);
                    callback({status: false, message: "Delete Blocked! License is invalid or expired"});
                }

                
            });


            client.on("update-link", async (data, callback)=>{
                data = JSON.parse(data);
                var key = data.key;
                var originalKey = data.originalKey;
                var folder = data.folder;
                var target = data.link;
                var recipients = data.recipients;
                var facebook_pixel = data.fbPixel;
                var link = await database.getRedirect(key);

                if(link.length!=0 && originalKey!=key) callback({status: false, key: data.key});
                else{

                    // Check if there are recipients
                    var sendEmail = false;
                    recipients.forEach(function(address){ if(address!="") sendEmail = true;})
                    
                    if(sendEmail && data.sendReport){
                        try{
                            await sendCampaignReport(recipients, originalKey);
                        }catch(err){
                           
                        }
                        
                    }

                    var links = await database.getCustomerLinks(client.id);
                    links = links[0].data;

                    if(links==undefined) links = [];
                    
                    links.forEach(function(link){
                        if(link.id==originalKey){
                            link.id = key;
                            link.target = target;
                            link.folder = folder;
                            link.recipients = recipients;
                            link.facebook_pixel = facebook_pixel;
                        }
                    });

                    
                    await database.updateCustomerLinks(client.id, links);
                    links = await database.getCustomerLinks(client.id);

                    
                    tool.logs(`(LINK UPDATE) /${data.key} >> SUCCESS`);
                    callback({status: true, links: links[0].data});
                }
            });


            client.on('disconnect',function(){
                tool.logs(`(DISCONNECT) ${client.id}`);
            });

        });
    }
}


async function validateLicense(id){
    let info = await database.getCustomer(id);
    
    var result; 
    if(info==false) result = {status: "false"};
    else{
        var temp = {};

        temp.email = info[0].email;
        temp.expiration = info[0].expiration_date;
        temp.last_payment = info[0].last_payment_date;
        temp.customer = info[0].customer;
        temp.data = info[0].data;

        result = {status: "true", data: temp};
        
        
    }

    return result;
}


async function sendCampaignReport(to, id){
    return new Promise(async (resolve, reject)=>{
    var links = await database.getRedirect(id);
    links = links[0].data;
    
    var uniqueDates = [];
    var uniqueIPAddress = [];
    
    // FIND SPECIFIC SHORT LINK
    links.forEach(function(link){
        if(link.id==id){

            if(link.logs==undefined || link.logs==null) link.logs = [];
            // LOOP THROUGH ALL LOGS
            link.logs.forEach(function(log){

                // GET ALL UNIQUE DATES
                log.tempDate = `${log.date.getFullYear()}-${log.date.getMonth()+1}-${log.date.getDate()}`;
                var isExist = false;
                uniqueDates.forEach((d)=>{if(d==log.tempDate) isExist = true;});
                (!isExist ? uniqueDates.push(log.tempDate) : false);


                // GET ALL UNIQUE IP ADDRESS
                var isExist = false;
                uniqueIPAddress.forEach((e)=>{if(e==log.ip) isExist = true;});
                (!isExist ? uniqueIPAddress.push(log.ip): false);
            });

            
            var __FINAL_DATA = {
                totalClicks: link.logs.length,
                totalUniqueClicks: uniqueIPAddress.length,
                dailyAverageClicks: (link.logs.length/uniqueDates.length)
            }

            if(__FINAL_DATA.totalClicks==0) __FINAL_DATA.dailyAverageClicks = 0;

            email.sendReport(to, {long: link.target, short: link.id}, __FINAL_DATA).then(()=>{
                tool.logs(`(SEND MAIL) ${to} >> SUCCESS`);
                resolve();
            }).catch(()=>{
                tool.logs(`(SEND MAIL) ${to} >> FAILED`);
                reject();
            });
        }
    });
    });
}