const express = require("express");
const app = express();  
const port = 8080;
var http = require('http'), io = require('socket.io');

const routes = require("./modules/routes");
const webhook = require("./modules/webhook");
const shortener = require("./modules/shortener");
const tool = require("./modules/tools");
const socket = require("./modules/socket");
const database = require("./modules/database");


init();
async function init(){
    await database.connect();

    app.use(require('body-parser').raw({type: '*/*'}));
    app.use("/webhook", webhook);
    app.use("/api", shortener);
    app.use("/", routes); 


    var server = http.createServer(app);
    server.listen(port, ()=>{ tool.logs(`Listening to port ${port}`);});
    socket.SOCKET(io.listen(server));
}
